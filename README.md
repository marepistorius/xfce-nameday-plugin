# Xfce Nameday Plugin

Xfce's panel's plugin for show namedays.



## Features

- Show Nameday of Day
- Show next [custom] number of namedays
- Search in namedays
- Show Date of Easter
- GTK3+ compactibile

  
## Screenshots

![App Screenshot](https://gitlab.com/marekpistorius/xfce-stuff/-/raw/master/xfce-nameday-plugin/screenshot/img.png)

  
## Tech Stack

Depends on GTK+ 3.x+ and Xfce's 'standarts' Dependiences

  
## Installation 

Install **Xfce-Nameday-Plugin** with 

```bash 
  /autogen.sh --prefix=/usr/
  make
  make install 
```
    
## Authors and Acknowledgement

- [@mmaniu](https://www.gitlab.com/marekpistorius) for development and design.

  
## Contributing

Contributions are always welcome!


  
## Feedback

If you have any feedback, please reach out to us at me@maniu.eu

  
